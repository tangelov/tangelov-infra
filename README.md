# tangelov-infra

Repository to store all the Terraform code related to the infrastructure of tangelov.me site

## How to use this code

### Dependencies

* Bash and jq
* Terraform 1.0 or higher
* TFsec
* Conftest

### Step by step

To replicate this infrastructure you have to do a add a little file to this code:

* Step 0: you should be in GNU/Linux, another Unix-like system or in the WSL of Microsoft Windows because this project use relative paths and they are not supported in Windows. It's easy to change it but it's not supported by default.

* First, you'll need to create a file called _env.tfvars_ and fill this variables with their own proper values. We recommend at least two environments:

```bash
# Provider configuration variables
gcp_default_project = "id-of-the-project"

# General variables
env                     = "full-name-of-the-environment"
gcp_environment         = "short-name-of-the-environment"
alt_environment         = "if-the-environment-needs-some-cross-permissions-set-this"
gcp_app_engine_location = "europe-west or other location"
gcp_app_engine_domain   = "domain-name-to-use-in-App-Engine"

# IAP variables
iap_support_email      = "a-email-you-controls"
iap_enabled_identities = [
    "user:who-can-access"
  ]

# Functions variables
gcp_bucket_functions_name = "name-for-the-functions-bucket"

# PubSub variables
orchestator_topic_name          = "name-for-the-orchestrator-pubsub-topic"
matrix_notifications_topic_name = "name-for-the-matrix-pubsub-topic"
cscheduler_backup_checker_cron  = "schedule-in-cron-style"

# IAM variables
sa_deployer_account_id    = "id-of-the-cloud-deployer-sa-account"
sa_deployer_display_name  = "name-of-the-cloud-deployer-sa-account"
sa_functions_account_id   = "id-of-the-pubsub-consumer-sa-account"
sa_functions_display_name = "name-of-the-pubsub-consumer-sa-account"

# Billing variables
billing_account_name      = "full-name-of-the-billing-account"
billing_dataset_name      = "name-of-the-bq-dataset-to-store-billing"
```

* If you want to use a Google Cloud Storage bucket to store the state of your Terraform deployments you should modify all the remote_state.tf files (one located per folder) and change the bucket name of the variable. If you doesn't want to use it, just delete it.

* After finishing all the steps before, you should put your Service account JSON in the root folder, naming it as _terraform.json_.

* Define variables GITLAB_TOKEN and CI_PROJECT_ID with proper values:

* Finally you only should enter in any folders you want and do:
    * ./dependencies.sh (if applies)
	* terraform init -backend-config=env.tfvars (if using remote state config, if not, don't use the bucket config parameter)
	* terraform plan -var-file=env.tfvars
	* terraform apply -var-file=env.tfvars


### Adding new functions to this code
Every Cloud Function has two different elements: the code used by the function to execute and the Terraform resources needed to put that code into the Cloud. In my infrastructure each element has one repository therefore we have to orchestrate the execution between both Gitlab repositories.

To add a new Cloud Function to Terraform you have to do the following steps:

1. Create a new CICD variable called CFUNCTION_$NAME to identify and track and sync the code version deployed. This variable has to be created in the Terraform repository.
2. Add the variable name to the dependencies.sh file in FUNCTION_VARS.
3. Copy the Terraform code of every function and modify it to create a new configuration. Commit all that changes.
4. Every time you create a tag in the source code of the function, you will be able to update your functions via Terraform and to track the changes without problem.


## Testing Terraform
This code use some code written in Rego that can be used to validate the results of any terraform plan. The tests are basic as they only validate that resources are not destroyed without control. To pass testing by hand, you have to create a terraform plan file in JSON format:

0. You have to download conftest from its official page, put it into your PATH and give it execute permissions. You can download it from [here](https://github.com/open-policy-agent/conftest/releases).

1. You have to create a terraform plan binary file. To do it, you have to execute the following commands ```terraform plan -var-file=env.tfvars -out terraform.plan```. If you need to validate a destroy, you can use _terraform taint_ and _terraform untaint_ temporarily to create a plan with that actions.

2. After creating the binary tfplan file, you need to convert it to JSON format. You can do it executing this command, using the previously binary file as input: ```terraform show -json terraform.plan > terraform-tfplan.json```

3. Validate the tests with Conftest using this command: ```conftest test --all-namespaces gcp/terraform-tfplan.json```


## Disclaimer
Terraform just initialize the App Engine service. It's not possible to add custom domain names or certificates (for example) with Terraform and they should be done via _gcloud_ CLI or using the Google Cloud console in your browser.

Good luck
