# Resources related to Cloud KMS
resource "google_project_service" "kms_api" {
  count   = var.env == "prd" ? 1 : 0
  project = data.google_project.tangelov_project.project_id
  service = "cloudkms.googleapis.com"

  disable_dependent_services = true
}


resource "google_kms_key_ring" "keyring" {
  count    = var.env == "prd" ? 1 : 0
  name     = var.gcp_default_region
  location = var.gcp_default_region

  depends_on = [
    google_project_service.kms_api
  ]
}

resource "google_kms_crypto_key" "sops" {
  count    = var.env == "prd" ? 1 : 0
  name     = "sops"
  key_ring = google_kms_key_ring.keyring[count.index].id
  purpose  = "ENCRYPT_DECRYPT"

  labels = merge(
    local.labels,
    { "app" : "sops" }
  )

  lifecycle {
    prevent_destroy = true
  }
}

resource "google_kms_crypto_key_iam_member" "sops" {
  count         = var.env == "prd" ? 1 : 0
  crypto_key_id = google_kms_crypto_key.sops[count.index].id
  role          = "roles/cloudkms.cryptoKeyEncrypterDecrypter"
  member        = "serviceAccount:${google_service_account.sops[count.index].email}"
}
