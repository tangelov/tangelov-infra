# Resources related to Google App Engine
resource "google_app_engine_application" "app_engine" {
  count       = 1
  project     = var.gcp_default_project
  location_id = var.gcp_app_engine_location

  # Configuring IAP in a dynamic way. Only if the environment is non-productive
  dynamic "iap" {
    for_each = var.env == "prd" ? [] : [var.env]
    content {
      enabled              = true
      oauth2_client_id     = google_iap_client.project_client[count.index].client_id
      oauth2_client_secret = google_iap_client.project_client[count.index].secret
    }
  }

  lifecycle {
    prevent_destroy = true
  }
}

resource "google_app_engine_domain_mapping" "tangelov" {
  domain_name       = var.gcp_app_engine_domain
  override_strategy = "STRICT"

  ssl_settings {
    ssl_management_type = "AUTOMATIC"
  }

  project = var.gcp_default_project
}