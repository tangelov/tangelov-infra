locals {
  labels = {
    environment     = var.gcp_environment
    management-tool = "terraform"
  }
}