variable "gcp_default_project" {
  type        = string
  description = "Default project of the whole environment"
}

variable "gcp_default_region" {
  type        = string
  default     = "europe-west1"
  description = "Default region of the whole environment"
}

# Non zonal resources right now
/*
variable "gcp_default_zone" {
  type        = string
  default     = "europe-west1-b"
  description = "Default zone of the whole environment"
}
*/

variable "env" {
  type        = string
  description = "Name of the GCP environment"
}

# Variables related to Google App Engine
variable "gcp_app_engine_location" {
  type        = string
  default     = "europe-west2"
  description = "Default GCP region to deploy in App Engine"
}

variable "gcp_app_engine_domain" {
  type        = string
  description = "Domain to be used in App Engine as primary domain"
}

# Variables related to the global tagging system
variable "alt_environment" {
  type        = string
  description = "Alternate environment to deploy any GCP resource to cross env access"
}

variable "gcp_environment" {
  type        = string
  default     = "production"
  description = "Default environment to deploy any GCP resource"
}

# Variables related to Identity Aware Proxy
variable "iap_support_email" {
  type        = string
  default     = "dummy@test.com"
  description = "Valid non-org email to contact in case of support"
}

variable "iap_enabled_identities" {
  type        = list(string)
  default     = [""]
  description = "List of identities with access to HTTPS resources"
}

# Variables relacionadas con Big Query
variable "gcp_bq_dataset_exports_id" {
  type        = string
  default     = "logging"
  description = "Dataset ID where the logging exports are going to be saved"
}

variable "gcp_bq_dataset_exports_name" {
  type        = string
  default     = "logging"
  description = "Name of the BigQuery dataset to save any logging exports"
}

variable "gcp_bq_dataset_exports_description" {
  type        = string
  default     = "This dataset will save all the logs of the GAE applications"
  description = "Description of the BigQuery dataset to save any logging exports"
}

variable "gcp_bq_dataset_exports_location" {
  type        = string
  default     = "EU"
  description = "Location by default for any BigQuery dataset"
}

# Public datasets are located in US
variable "gcp_bq_dataset_exports_alt_location" {
  type        = string
  default     = "US"
  description = "Alternate location for BigQuery datasets that needs to use public data"
}

variable "gcp_bq_dataset_exports_data_transfer_dname" {
  type        = string
  default     = "dataset-to-us"
  description = "Name of the BigQuery export that transfers data between regions"
}

# Variables relacionadas con los exports de Stackdriver
variable "gcp_project_sink_gae_requests_name" {
  type        = string
  default     = "gae_logging"
  description = "Name of the Stackdriver sink that puts data into BigQuery to count visits"
}

# Variables relacionadas con Cloud Storage
variable "gcp_bucket_images_name" {
  type        = string
  default     = "tangelov-data"
  description = "Name of the bucket where the images are stored"
}

variable "gcp_bucket_functions_name" {
  type        = string
  description = "Name of the bucket where the functions dependencies are stored"
}

variable "gcp_bucket_images_storage_class" {
  type        = string
  default     = "REGIONAL"
  description = "Type of the bucktet in GCS where the images are stored"
}

# Variables relacionadas con Cloud Functions
variable "cfunction_messages_matrix_name" {
  type        = string
  default     = "messages_to_matrix"
  description = "Name of the Messages to Matrix function"
}

variable "cfunction_checking_billing_name" {
  type        = string
  default     = "checking_billing"
  description = "Name of the Checking Billing function"
}

# Variables relacionadas con PubSub
variable "orchestator_topic_name" {
  type        = string
  description = "Name of the topic where Cloud scheduler events will be stored"
}

variable "matrix_notifications_topic_name" {
  type        = string
  description = "Name of the topic where Matrix notifications will be stored"
}

# IAM variables

# Variables for Cloud Deployer service account
variable "sa_deployer_account_id" {
  type        = string
  description = "Identifier for GAE deployer service account"
}

variable "sa_deployer_display_name" {
  type        = string
  description = "Display name for the GAE deployer service account"
}

variable "sa_deployer_description" {
  type        = string
  default     = "This account will be used to deploy in GAE with CICDs processes  in the Cloud"
  description = "Description of the service account"
}

# Variables for Cloud Functions service account
variable "sa_functions_account_id" {
  type        = string
  description = "Identifier for Google Cloud Functions service account"
}

variable "sa_functions_display_name" {
  type        = string
  description = "Display name for Google Cloud Functions service account"
}

variable "sa_functions_description" {
  type        = string
  default     = "This account will be used to send messages via functions and PubSub in the Cloud"
  description = "Description of the service account"
}

# Variables for SOPS service account
variable "sa_sops_account_id" {
  type        = string
  default     = "dummy"
  description = "Identifier for SOPS service account"
}

variable "sa_sops_display_name" {
  type        = string
  default     = "dummy"
  description = "Display name for SOPS service account"
}

variable "sa_sops_description" {
  type        = string
  default     = "This account will be used to encrypt and decrypt secrets in the Cloud."
  description = "Description of the service account"
}

# Variables for External service account
variable "sa_external_account_id" {
  type        = string
  default     = "dummy"
  description = "Identifier for External service account"
}

variable "sa_external_display_name" {
  type        = string
  default     = "dummy"
  description = "Display name for External service account"
}

variable "sa_external_description" {
  type        = string
  default     = "This account will be used to send messages to PubSub outside of Google Cloud"
  description = "Description of the service account"
}


# Variables relacionadas con Cloud Scheduler
variable "cscheduler_backup_checker_name" {
  type        = string
  default     = "backup-checker"
  description = "Name of the job in Cloud Scheduler to check the status of the backups"
}

variable "cscheduler_backup_checker_description" {
  type        = string
  default     = "This job will check if backups are performing well"
  description = "Description of the job in Cloud Scheduler to check the status of the backups"
}

variable "cscheduler_backup_checker_cron" {
  type        = string
  default     = ""
  description = "Cron to execute backup checker in a regular basis"
}

variable "cscheduler_backup_checker_timezone" {
  type        = string
  default     = "Europe/Madrid"
  description = "Timezone to use in the cron of Cloud Scheduler"
}

variable "cscheduler_backup_checker_payload" {
  type        = string
  default     = "backup-checker"
  description = "Data for the messages to put in PubSub by Backup Checker Cloud Scheduler job"
}

# Variables relacionadas con Topics y Subscriptions
/*
variable "matrix_client_subscription_name" {
  type        = string
  default     = "matrix-client"
  description = "Name of the subscription where the Matrix notification are getting consumed"
}
*/

# Variables relacionadas con Billing
variable "billing_account_name" {
  type        = string
  description = "Display name of the Billing Account"
}

variable "billing_dataset_name" {
  type        = string
  description = "Name of the BigQuery dataset created to store the Biling export"
}