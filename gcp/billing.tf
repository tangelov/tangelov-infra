# Enabling Billing API in GCP projects
resource "google_project_service" "billing_api" {
  project = data.google_project.tangelov_project.project_id
  service = "cloudbilling.googleapis.com"

  disable_dependent_services = true
}

data "google_billing_account" "tangelov" {
  display_name = var.billing_account_name
  open         = true

  depends_on = [google_project_service.billing_api]
}
